FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=7.11.15

RUN cd /tmp && \
    wget https://suitecrm.com/files/162/SuiteCRM-7.11/513/SuiteCRM-${VERSION}.zip && \
    unzip SuiteCRM-${VERSION}.zip -d /tmp && \
    mv /tmp/SuiteCRM-${VERSION}/* /app/code && \
    rm -rf SuiteCRM-${VERSION}.zip /tmp/SuiteCRM-${VERSION}

# RUN php /usr/local/bin/composer install --no-plugins --no-scripts --no-interaction --prefer-dist --no-suggest --no-dev --optimize-autoloader

# setup directories  (https://docs.suitecrm.com/developer/suitecrm-directory-structure/)
RUN mv /app/code/custom /app/code/custom.orig && ln -sf /app/data/custom /app/code/custom
RUN mv /app/code/cache /app/code/cache.orig && ln -sf /run/suitecrm/cache /app/code/cache
RUN mv /app/code/modules /app/code/modules.orig
RUN rm -rf /app/code/upload && ln -sf /app/data/upload /app/code/upload
RUN ln -s /app/data/config.php /app/code/config.php
RUN ln -s /run/suitecrm/suitecrm.log /app/code/suitecrm.log && \
    ln -s /run/suitecrm/status.json /app/code/install/status.json

RUN chown -R www-data:www-data /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/suitecrm.conf /etc/apache2/sites-enabled/suitecrm.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN a2enmod rewrite && \
    a2enmod expires && \
    a2enmod headers && \
    a2enmod cache && \
    a2enmod php7.3

# configure mod_php
RUN crudini --set /etc/php/7.3/apache2/php.ini PHP upload_max_filesize 50M && \
    crudini --set /etc/php/7.3/apache2/php.ini PHP post_max_size 50M && \
    crudini --set /etc/php/7.3/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.3/apache2/php.ini PHP max_input_vars 1800 && \
    crudini --set /etc/php/7.3/apache2/php.ini Session session.save_path /run/suitecrm/sessions && \
    crudini --set /etc/php/7.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.3/apache2/php.ini Session session.gc_divisor 100

COPY start.sh /app/code/
COPY htaccess /app/code/.htaccess
COPY config_si.php /app/code/config_si.php

CMD [ "/app/code/start.sh" ]
