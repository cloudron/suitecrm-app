This app packages SuiteCRM 7.10.7

### Overview

SuiteCRM is a software fork of the popular Customer Relationship Management (CRM) system SugarCRM, developed
and maintained by SalesAgility. It is a free and open source alternative application.

SuiteCRM has been downloaded more than 500,000 times since the original release. It has been adopted by
NHS (National Health Service) England’s Code for Health programme which seeks to foster open source in the NHS in England.

The SuiteCRM project has stated that the every line of code released by the project will be open source. SuiteCRM project
is intended to be an enterprise-class open source alternative to proprietary products.

A project Roadmap is available that details planned enhancements.

An active public support forum with more than 25,000 members is available for free support and is regularly monitored and
updated by the Project team.

A directory of extensions is available where both free and paid-for enhancements are available.

