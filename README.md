# SuiteCRM Cloudron App

This repository contains the Cloudron app package source for [SuiteCRM](https://suitecrm.com)

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=com.suitecrm.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id com.suitecrm.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd suitecrm-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, backup and restore. 

```
cd suitecrm-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

