#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */
/* global Promise:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    url = require('url');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var server, browser = new Builder().forBrowser('chrome').build();
    var LOCATION = 'test';
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var email = process.env.EMAIL;
    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));
        if (!process.env.EMAIL) return done(new Error('EMAIL env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function skipUserWizard() {
        return browser.wait(until.elementLocated(by.id('next_tab_personalinfo')), TIMEOUT).then(function () {
            return browser.findElement(by.id('next_tab_personalinfo')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath(`//input[@value="${email}"]`)), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.id('next_tab_locale')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.id('next_tab_finish')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.id('next_tab_finish')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//input[@title="Finish"]')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@title="Finish"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[contains(text(),"SUITECRM DASHBOARD")]')), TIMEOUT);
        });
    }

    function login(username, password, skipWizard, done) {
        browser.manage().deleteAllCookies().then(function () {
            return browser.get('https://' + app.fqdn);
        }).then(function () {
            return browser.sleep(2000);
        }).then(function () {
            return browser.wait(until.elementLocated(by.id('user_name')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.id('user_name')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.id('username_password')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@value="Log In"]')).submit();
        }).then(function () {
            if (skipWizard) return skipUserWizard();
            return Promise.resolve();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[contains(text(),"SUITECRM DASHBOARD")]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function adminLogin(done) {
        login('admin', 'changeme', false /* skipUserWizard */, done);
    }

    var contactId;
    function createContact(done) {
        browser.get('https://' + app.fqdn + '/index.php?module=Contacts&action=EditView&return_module=Contacts&return_action=DetailView').then(function () {
            return browser.wait(until.elementLocated(by.id('last_name')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.id('last_name')).sendKeys('cloudroncontact');
        }).then(function () {
            return browser.findElement(by.id('SAVE')).click();
        }).then(function () {
            return browser.wait(function () {
                return browser.getCurrentUrl().then(function (u) {
                    contactId = url.parse(u, true /* parse query */).query.record;
                    return !!contactId;
                });
            }, TIMEOUT);
        }).then(function () {
            console.log(`Contact ID is ${contactId}`);
            done();
        });
    }

    function contactExists(done) {
        browser.get(`https://${app.fqdn}/index.php?action=DetailView&module=Contacts&record=${contactId}`).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//span[text()="cloudroncontact"]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function logout(done) {
        browser.get('https://' + app.fqdn + '/index.php?module=Users&action=Logout').then(function () {
            return browser.sleep(3000);
        }).then(function () {
            done();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function (done) {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        setTimeout(done, 5000); // give sometime for setup to complete
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password, true /* skipUserWizard */));
    it('can create contact', createContact);
    it('can check contact', contactExists);
    it('can logout', logout);

    it('can restart app', function (done) {
        execSync('cloudron restart --wait --app ' + app.id);
        done();
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password, false /* skipUserWizard */));
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password, false /* skipUserWizard */));
    it('can check contact', contactExists);
    it('can logout', logout);

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password, false /* skipUserWizard */));
    it('can check contact', contactExists);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password, false /* skipUserWizard */));
    it('can check contact', contactExists);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function (done) {
        execSync('cloudron install --new --wait --appstore-id com.suitecrm.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
        setTimeout(done, 5000); // give sometime for setup to complete
    });
    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password, true /* skipUserWizard */));
    it('can skip wizard', skipUserWizard);
    it('can create contact', createContact);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can check contact', contactExists);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});

