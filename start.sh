#!/bin/bash

set -eu -o pipefail

function setup() {
    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "Waiting for apache2 to start"
        sleep 1
    done

    echo "Running first time setup"
    touch /app/data/config.php && chown www-data:www-data /app/data/config.php
    if ! curl --fail -v 'http://localhost:8000/install.php?goto=SilentInstall&cli=true'; then
        echo "Failed to setup"
        return 1
    fi

    touch /app/data/.setup
}

function update_config() {
    echo "Setting up LDAP authentication"

    # the config table has no primary key, so it has to be cleared first
    # we don't set admin_user and admin_password for bind credentials yet because it is stored encrypted
    # using blowfish with the keys from custom/blowfish/rapelcg_svryq.php
    if ! mysql -u"${MYSQL_USERNAME}" -p"${MYSQL_PASSWORD}" -h "${MYSQL_HOST}" --database="${MYSQL_DATABASE}"  \
        -e "DELETE FROM config WHERE category='ldap';" \
        -e "INSERT INTO config (category, name, value) VALUES ('system', 'ldap_enabled', '1');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'authentication', '1');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'auto_create_users', '1');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'hostname', '${LDAP_SERVER}');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'port', '${LDAP_PORT}');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'base_dn', '${LDAP_USERS_BASE_DN}');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'login_filter', '');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'bind_attr', 'dn');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'login_attr', 'username');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'authentication_checkbox', 'on');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'group_dn', '');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'group_name', '');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'group_user_attr', '');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'group_attr', '');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'group', '0');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'group_attr_req_dn', '0');" \
        -e "INSERT INTO config (category, name, value) VALUES ('ldap', 'enc_key', '');" ; then
        echo "Failed to setup LDAP auth"
        return 1
    fi
}

echo "== Startup =="

# https://docs.suitecrm.com/developer/suitecrm-directory-structure/
echo "Ensure directories"
mkdir -p /run/suitecrm/sessions /run/suitecrm/cache /app/data/upload /app/data/custom /app/data/modules
touch /run/suitecrm/suitecrm.log

echo "Creating custom directory"
if [[ ! -f /app/data/custom ]]; then
    cp -r /app/code/custom.orig/* /app/data/custom
fi

# note that /app/data/modules is bindMounted via the CloudronManifest.json to make the relative paths to work
echo "Overwriting upstream modules"
for f in $(ls /app/code/modules.orig); do
    rm -rf "/app/data/modules/$f"
    cp -r /app/code/modules.orig/$f /app/data/modules/$f
done

echo "Copying cache directory"
rm -rf /run/suitecrm/cache/* && cp -r /app/code/cache.orig/* /run/suitecrm/cache

echo "Ensure directory permissions"
chown -R www-data:www-data /app/data /run/suitecrm

if [[ ! -f /app/data/.setup ]]; then
    echo "Starting apache for setup"
    APACHE_CONFDIR="" source /etc/apache2/envvars
    rm -f "${APACHE_PID_FILE}"
    ( /usr/sbin/apache2 -DFOREGROUND ) &
    apache_pid=$!
    setup
    echo "Setup complete"
    kill ${apache_pid}
fi

if ! update_config; then
    echo "Could not update configuration"
    exit 1
fi

echo "Starting SuiteCRM"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

